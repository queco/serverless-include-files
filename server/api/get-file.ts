export default defineEventHandler(async () => {
  const assets = useStorage("assets:server");
  return await assets.getItem("file.txt");
});
